<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
protected $table = 'images';
protected $fillable=[
    'id',
    'url',
];
public function post(){
    return $this->hasOne('App\Posts');
}
}
