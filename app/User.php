<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\UsersController;

class User extends Model
{
    protected $with = ['posts'];
    protected $table='users';
    protected $fillable=[
        'name',
        'email',
        'password',
        'active',
        'created_at',
        'updated_at',
    ];
    /**
     *
     */
    public function posts()
    {
        return $this->hasMany('App\Posts', 'author_id');
    }

}
