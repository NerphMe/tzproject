<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $table='comments';
    protected $fillable=[
        'content',
        'created_at',
        'update_at',
        'deleted_at',
        ];

}
