<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function users()
    {
        return response()->json(User::get(), 200);
    }

    public function usersByID($id)
    {
        $users = User::find($id);
        if (is_null($users)) {
            return response()->json(["message"=>"nothing"], 404);
        }
        return respone()->json($users,200);
    }

    public function usersSave(Request $request)
    {
        $users= User::create($request->all());
        return response()->json($users, 201);
    }

    public function usersUpdate(Request $request, $id)
    {
        $users = User::find($id);
        if (is_null($users)) {
            return response()->json('Nothing', 404);

        }
        $id->update($request->all());
        return response()->json($id, 200);
    }

    public function usersDelete(Request $request, $id)
    {
        $users = User::find($id);
        if (is_null($users)) {
            return response()->json('Nothing', 404);

        }
        $users->delete();
        return response()->json(null, 204);
    }
}
