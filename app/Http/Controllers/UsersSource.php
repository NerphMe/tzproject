<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsersSource extends Controller
{
    /// 127.0.0.1:8000/api/users || GET
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = [];
        foreach (User::all() as $user ){
            $userApiData = [];
            $userApiData['id'] = $user->id;
            $userApiData['name'] = $user->name;

            $posts = [];

            foreach ($user->posts as $post) {
                $postApiData = [];
                $postApiData['id'] = $post->id;
                $postApiData['content'] = $post->content;
                $postApiData['created_at_ts'] = $post->created_at_ts;
                $image = $post->image;
                if ($image !== null) {
                    $postApiData['image_url'] = $image->url;
                }

                $posts[] = $postApiData;

                $userApiData['posts'][] = $posts;

            }

            $users[] = $userApiData;
        }
        $response = [];
        $response['data'] = $users;

        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /// 127.0.0.1:8000/api/users || GET

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $users = User::create($request->all());
        return response()->json($users, 201);
    }
    /// 127.0.0.1:8000/api/users/{id} поиск по id ||GET

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = User::find($id);
        if (is_null($users)) {
            return response()->json('Nothing', 404);
        }
        return response()->json($users, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }
    /// 127.0.0.1:8000/api/users{id} || UPDATE

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $users = User::find($id);
        if (is_null($users)) {
            return response()->json('Nothing', 404);

        }
        $id->update($request->all());
        return response()->json($id, 200);
    }
    /// 127.0.0.1:8000/api/users{id} || DELETE

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $users = User::find($id);
        if (is_null($users)) {
            return response()->json('Nothing', 404);

        }
        $users->delete();
        return response()->json(null, 204);
    }
}
