<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
//    protected $with = ['comments'];
    protected $table='posts';
    protected $fillable=[
        'content',
        'created_at',
        'update_at',
        'deleted_at',
    ];
    public function comments(){
        return $this->hasMany('App\Comments', 'id');
    }
    public function image(){
        return $this->hasOne('App\Image', 'post_id');
    }
}
