<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersSeeds::class);
        $this->call(postsSeeds::class);
        $this->call(commentsSeed::class);
        $this->call(imagesSeeds::class);


    }
}

