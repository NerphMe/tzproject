<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class commentsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert([
            'post_id' => '1',
            'commentator_id' => '1',
            'content' => 'sdasdaasdasda',
            'created_at' => Carbon::parse(),
            'updated_at' => Carbon::parse(),
            'deleted_at' => Carbon::parse(),
        ]);

    }
}
