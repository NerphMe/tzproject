<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersSeeds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'dsada',
            'email' => '3213@gmail.com',
            'password' => '2321',
            'active' => true,
            'created_at' => Carbon::parse(),
            'updated_at' => Carbon::parse(),
            ]);
    }
}
