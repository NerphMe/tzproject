<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class postsSeeds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            [
                'id' => '1',
                'author_id' => '1'  ,
                'content' => 'qwerty',
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);

    }
}

