<?php

use Illuminate\Database\Seeder;

class imagesSeeds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('images')->insert([
            'url' => 'bigmir.ua',
            'post_id'=> '2',
        ]);
    }
}
